﻿# Model 3D on FreeCad
============

# Images
![3D_photo_1](/Model3D/Cable_chain/Cable_chain_adapter_XAxis_1.jpg)
![3D_photo_2](/Model3D/Cable_chain/Cable_chain_adapter_XAxis_2.jpg)

# License
======

[Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/)

# Attributions
============
See commit details to find the authors of each Part.
- @fandres323
